import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class DefaultButton extends StatelessWidget {
  final Function onPressed;
  final Widget child;

  DefaultButton({this.onPressed, this.child});
  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      height: ScreenUtil().setHeight(155),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(5),
          bottomLeft: Radius.circular(20),
          topRight: Radius.circular(20),
          bottomRight: Radius.circular(5),
        ),
      ),
      color: Color.fromARGB(255, 126, 217, 208),
      onPressed: this.onPressed,
      child: child,
    );
  }
}
