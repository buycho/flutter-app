import 'package:bubble_bottom_bar/bubble_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class BottomBar extends StatelessWidget {
  final Function onTap;
  final int pageIndex;

  BottomBar({@required this.onTap, @required this.pageIndex});

  @override
  Widget build(BuildContext context) {
    return BubbleBottomBar(
      backgroundColor: Colors.white,
      opacity: 0,
      elevation: 0,
      currentIndex: this.pageIndex,
      onTap: this.onTap,
      borderRadius: BorderRadius.only(topLeft: Radius.circular(20)),
      items: <BubbleBottomBarItem>[
        BubbleBottomBarItem(
          backgroundColor: Colors.pinkAccent,
          icon: Icon(FontAwesomeIcons.compass, color: Colors.grey[900]),
          activeIcon: Icon(FontAwesomeIcons.compass, color: Colors.pinkAccent),
          title: Text(""),
        ),
        BubbleBottomBarItem(
          backgroundColor: Colors.pinkAccent,
          icon: Icon(FontAwesomeIcons.home, color: Colors.grey[900]),
          activeIcon: Icon(FontAwesomeIcons.home, color: Colors.pinkAccent),
          title: Text(""),
        ),
        BubbleBottomBarItem(
          backgroundColor: Colors.pinkAccent,
          icon: Icon(FontAwesomeIcons.rss, color: Colors.grey[900]),
          activeIcon: Icon(FontAwesomeIcons.rss, color: Colors.pinkAccent),
          title: Text(""),
        ),
        BubbleBottomBarItem(
          backgroundColor: Colors.pinkAccent,
          icon: Icon(FontAwesomeIcons.personBooth, color: Colors.grey[900]),
          activeIcon:
              Icon(FontAwesomeIcons.personBooth, color: Colors.pinkAccent),
          title: Text(""),
        )
      ],
    );
  }
}
