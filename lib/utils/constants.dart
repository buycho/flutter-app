import 'package:flutter/material.dart';

const kDefaultFontFamily = "Josefin Sans";

const kBackgroundColor = Colors.grey;
const kPrimaryColor = Colors.pink;

const kLoginLabelStyle = TextStyle(
  fontSize: 20,
  fontWeight: FontWeight.w600,
  fontFamily: kDefaultFontFamily,
);

const kLoginForgotPasswordStyle = TextStyle(
  decoration: TextDecoration.underline,
  fontWeight: FontWeight.w300,
  fontFamily: kDefaultFontFamily,
);

const kDefaultButtonTextStyle = TextStyle(
  fontWeight: FontWeight.w700,
  fontSize: 20,
  fontFamily: kDefaultFontFamily,
);
