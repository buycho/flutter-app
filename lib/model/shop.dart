class Shop {
  final String name;
  final String logo;
  final List clothes;

  Shop({this.name, this.logo, this.clothes});

  factory Shop.fromJson(Map<String, dynamic> json) {
    return new Shop(
      name: json['name'] as String,
      logo: json['logo'] as String,
      clothes: json['clothes'] == null ? [] : json['clothes'],
    );
  }
}
