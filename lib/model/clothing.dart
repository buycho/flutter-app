class Clothing {
  final String name;
  final double price;
  final String image;
  final String description;

  Clothing({this.name, this.price, this.image, this.description});

  factory Clothing.fromJson(Map<String, dynamic> json) {
    return new Clothing(
      name: json['name'] as String,
      price: json['price'],
      image: json['image'],
      description: json['description']
    );
  }
}
