import 'package:flutter/material.dart';
import 'package:instabreshop/screens/home/home_screen.dart';
import 'package:instabreshop/screens/login/login_screen.dart';

void main() => runApp(BuyCho());

class BuyCho extends StatelessWidget {
  ThemeData _buildThemeData() {
    // TO-DO: Como usar theme data pra gerenciar tema no app todo?
    return ThemeData(
      fontFamily: "Josefin Sans",
      primaryColor: Color.fromARGB(255, 230, 230, 230),
      accentColor: Color.fromARGB(255, 252, 178, 174),

      textTheme: TextTheme(
        title: TextStyle(),
        
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'BuyCho',
      theme: _buildThemeData(),
      debugShowCheckedModeBanner: false,
      home: LoginScreen(),
    );
  }
}
