import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:instabreshop/model/shop.dart';
import 'package:instabreshop/screens/home/widgets/home_screen_clipper.dart';
import 'package:instabreshop/screens/home/widgets/shop_display.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<Shop> _parseJson(jsonContent) {
    if (jsonContent == null) return [];

    final jsonDecoded = json.decode(jsonContent);
    if (jsonDecoded == null) return [];

    final result = jsonDecoded.cast<Map<String, dynamic>>();
    return result.map<Shop>((shopJson) => new Shop.fromJson(shopJson)).toList();
  }

  Future<List<Shop>> _getShops() async {
    String jsonContent = await DefaultAssetBundle.of(context)
        .loadString('assets/mocks/shops.json');
    List<Shop> shops = _parseJson(jsonContent);
    return shops;
  }

  @override
  void initState() {
    super.initState();
  }

  List<Widget> _mapListToWidget(List<Shop> shops) {
    return shops
        .map(
          (shop) => Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: ScreenUtil().setWidth(100)),
                child: Row(
                  children: <Widget>[
                    Image.asset("assets/mocks/${shop.logo}"),
                    Text(
                      shop.name,
                      style: TextStyle(
                        fontSize: 20,
                        fontFamily: "Josefin Sans"
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(),
                child: ShopDisplay(shop: shop),
              ),
            ],
          ),
        )
        .toList();
  }

  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: HomeScreenClipper(),
      child: Container(
        color: Colors.grey[300],
        child: Stack(
          children: <Widget>[
            _buildTitle(),
            FutureBuilder(
              initialData: List<Shop>(),
              future: _getShops(),
              builder: (context, snapshot) {
                return Padding(
                  padding: EdgeInsets.only(top: 75),
                  child: Column(
                    children: <Widget>[
                      ..._mapListToWidget(snapshot.data),
                    ],
                  ),
                );
              },
            )
          ],
        ),
      ),
    );
  }

  Positioned _buildTitle() {
    return Positioned(
      left: ScreenUtil().setWidth(100),
      top: ScreenUtil().setHeight(120),
      child: Text(
        "Seu Radar",
        style: TextStyle(
          fontSize: 30,
          fontWeight: FontWeight.w600,
        ),
      ),
    );
  }
}
