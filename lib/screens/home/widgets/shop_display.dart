import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:instabreshop/model/clothing.dart';
import 'package:instabreshop/model/shop.dart';
import 'package:instabreshop/screens/post/post_screen.dart';

class ShopDisplay extends StatelessWidget {
  final Shop shop;

  ShopDisplay({this.shop});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SizedBox(
        height: ScreenUtil().setHeight(550),
        child: ListView.builder(
          physics: BouncingScrollPhysics(),
          scrollDirection: Axis.horizontal,
          itemCount: shop == null ? 0 : this.shop.clothes.length,
          itemBuilder: (context, index) {
            final clothing = this.shop.clothes[index];
            Clothing obj = Clothing.fromJson(clothing);
            return _buildCard(obj, index, context);
          },
        ),
      ),
    );
  }

  Widget _buildCard(Clothing clothing, int index, BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => PostScreen(
              clothing: clothing,
              shop: shop,
            ),
          ),
        );
      },
      child: Container(
        margin: EdgeInsets.only(left: index == 0 ? 20 : 0, top: 5),
        width: ScreenUtil().setWidth(350),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Center(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(15),
                child: Image.asset(
                  "assets/mocks/${clothing.image}",
                  width: ScreenUtil().setWidth(300),
                ),
              ),
            ),
            Text(
              clothing.name,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(fontFamily: "Josefin Sans"),
            ),
            Text(
              "R\$ ${clothing.price}",
              style: TextStyle(fontFamily: "Josefin Sans", color: Colors.pink),
            ),
          ],
        ),
      ),
    );
  }
}
