import 'package:flutter/material.dart';

class HomeScreenClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    path.moveTo(20, 30);
    path.quadraticBezierTo(0, 30, 0, 60);

    path.lineTo(0, size.height);

    path.lineTo(size.width-15, size.height);
    path.quadraticBezierTo(size.width-5, size.height-5, size.width, size.height-12);

    path.lineTo(size.width, 20);
    path.quadraticBezierTo(size.width, 30, size.width-20, 30);

    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }

}