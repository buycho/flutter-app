import 'package:bubble_bottom_bar/bubble_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:instabreshop/screens/home/home_screen.dart';
import 'package:instabreshop/widgets/bottom_bar.dart';
import 'package:instabreshop/widgets/top_bar.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int _pageIndex = 0;
  PageController _pageController;

  @override
  void initState() {
    super.initState();
    _pageIndex = 0;
    _pageController = PageController();
  }

  void _onTabChange(int index) {
    print("Page Index: $index");
    setState(() {
      _pageIndex = index;
      _pageController.animateToPage(
        index,
        duration: Duration(milliseconds: 500),
        curve: Curves.ease,
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: Container(
        child: Stack(
          children: <Widget>[
            TopBar(),
            Padding(
              padding: EdgeInsets.only(top: 55),
              child: PageView(
                controller: _pageController,
                onPageChanged: (int index) {
                  setState(() {
                    _pageIndex = index;
                  });
                },
                children: <Widget>[
                  HomeScreen(),
                  Container(),
                  Container(),
                  Container(),
                  Container(),
                ],
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomBar(
        onTap: _onTabChange,
        pageIndex: _pageIndex,
      ),
    );
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }
}
