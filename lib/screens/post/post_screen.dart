import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:instabreshop/model/clothing.dart';
import 'package:instabreshop/model/shop.dart';
import 'package:instabreshop/screens/post/widgets/bottom_bar.dart';
import 'package:instabreshop/screens/post/widgets/top_bar.dart';
import 'package:instabreshop/utils/constants.dart';
import 'package:instabreshop/widgets/default_button.dart';

class PostScreen extends StatelessWidget {
  final Clothing clothing;
  final Shop shop;

  PostScreen({this.clothing, this.shop});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: SafeArea(
        child: Container(
          child: Column(
            children: <Widget>[
              TopBar(shop: shop),
              Container(
                color: Colors.white,
                width: ScreenUtil().setWidth(1125),
                child: Align(
                  alignment: Alignment.topCenter,
                  child: Image.asset(
                    "assets/mocks/${clothing.image}",
                    scale: 0.5,
                  ),
                ),
              ),
              new PostBody(clothing: clothing),
            ],
          ),
        ),
      ),
      bottomNavigationBar: BottomBar(),
    );
  }
}

class PostBody extends StatelessWidget {
  const PostBody({
    Key key,
    @required this.clothing,
  }) : super(key: key);

  final Clothing clothing;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(15),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    clothing.name,
                    style: TextStyle(
                      fontFamily: "Josefin Sans",
                      fontSize: 25,
                    ),
                  ),
                  Text(
                    "R\$ ${clothing.price}",
                    style: TextStyle(
                      fontFamily: "Josefin Sans",
                      color: Colors.pinkAccent,
                      fontSize: 20,
                    ),
                  )
                ],
              ),
              Icon(FontAwesomeIcons.solidHeart),
              Icon(FontAwesomeIcons.shareAlt),
              Icon(FontAwesomeIcons.solidBell),
            ],
          ),
          Text(
            clothing.description,
            style: TextStyle(
              fontFamily: "Josefin Sans",
              fontSize: 18,
            ),
          ),
          DefaultButton(
            onPressed: () {},
            child: Text("Eu Quero!", style: kDefaultButtonTextStyle),
          ),
        ],
      ),
    );
  }
}




