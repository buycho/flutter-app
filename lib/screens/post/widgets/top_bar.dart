import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:instabreshop/model/shop.dart';

class TopBar extends StatelessWidget {
  const TopBar({
    Key key,
    @required this.shop,
  }) : super(key: key);

  final Shop shop;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(left: 8, top: 10),
          child: Row(
            children: <Widget>[
              Image.asset("assets/mocks/${shop.logo}"),
              Text(
                shop.name,
                style: TextStyle(
                  fontSize: 18,
                  fontFamily: "Josefin Sans",
                ),
              ),
            ],
          ),
        ),
        GestureDetector(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: Container(
            width: ScreenUtil().setWidth(290),
            height: ScreenUtil().setHeight(300),
            decoration: BoxDecoration(
              color: Theme.of(context).accentColor,
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(40),
              ),
            ),
            child: Icon(
              FontAwesomeIcons.arrowLeft,
              color: Colors.white,
            ),
          ),
        )
      ],
    );
  }
}