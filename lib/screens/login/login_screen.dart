import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:instabreshop/screens/login/widgets/circular_icon_button.dart';
import 'package:instabreshop/screens/login/widgets/input_field.dart';
import 'package:instabreshop/screens/main/main_screen.dart';
import 'package:instabreshop/utils/constants.dart';
import 'package:instabreshop/widgets/default_button.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance = ScreenUtil(
      allowFontScaling: true,
      width: 1125,
      height: 2436,
    )..init(context);

    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: SafeArea(
        child: Align(
          alignment: Alignment.center,
          child: SingleChildScrollView(
            child: Container(
              margin: EdgeInsets.all(20),
              child: Column(
                children: <Widget>[
                  _buildBuychoLogo(),
                  /* Email */
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Text("email", style: kLoginLabelStyle),
                      InputField(hint: "Email"),
                    ],
                  ),
                  /* Password */
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Text("senha", style: kLoginLabelStyle),
                      InputField(hint: "Senha"),
                    ],
                  ),
                  /* Esqueceu a Senha */
                  Text(
                    "esqueceu a senha?",
                    textAlign: TextAlign.center,
                    style: kLoginForgotPasswordStyle,
                  ),
                  SizedBox(
                    height: ScreenUtil().setHeight(200),
                  ),
                  /* Login Button */
                  DefaultButton(
                    onPressed: () {
                      Navigator.of(context).pushReplacement(
                        MaterialPageRoute(builder: (context) => MainScreen()),
                      );
                    },
                    child: Text(
                      "Entrar",
                      style: kDefaultButtonTextStyle,
                    ),
                  ),
                  Divider(),
                  /* Insta Logo */
                  CircularIconButton(icon: FontAwesomeIcons.instagram)
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Padding _buildBuychoLogo() {
    return Padding(
      padding: EdgeInsets.only(bottom: ScreenUtil().setHeight(300)),
      child: SvgPicture.asset(
        'assets/images/buycho_logo.svg',
        semanticsLabel: "BuyCho Logo",
        width: ScreenUtil().setWidth(500),
      ),
    );
  }
}
