import 'package:flutter/material.dart';

class CircularIconButton extends StatelessWidget {
  final IconData icon;

  CircularIconButton({this.icon});
  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      onPressed: () {},
      elevation: 5,
      shape: CircleBorder(),
      fillColor: Colors.white,
      padding: EdgeInsets.all(10),
      child: Icon(
        this.icon,
        color: Colors.pinkAccent,
        size: 30,
      ),
    );
  }
}
