import 'package:flutter/material.dart';

class InputField extends StatelessWidget {
  final String hint;
  final String text;
  final bool obscure;

  InputField({@required this.hint, this.obscure = false, this.text});

  @override
  Widget build(BuildContext context) {
    return TextField(
      decoration: InputDecoration(
        hintText: this.hint,
        border: OutlineInputBorder(
          borderSide: BorderSide(
            color: Colors.blue,
            width: 40,
          ),
        ),
      ),
    );
  }
}
